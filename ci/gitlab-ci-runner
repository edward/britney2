#!/bin/bash

set -e

err=0

PYTEST_COV_OPTIONS=
BRITNEY=./britney.py
TEST_FLAGS=nocodestyle
SUBMODULES=
TEST_SUITE=britney2-tests/t
TEST=
BRANCH=master

while [ "$#" -gt 0 ]; do
    case "$1" in
        (--live)
            echo "Detecting live run: $2"
            SUBMODULES="--shallow-submodules --recurse-submodules"
            TEST_SUITE=britney2-tests/live-data
            TEST=live-${2}
            shift 2
            ;;
        (--with-coverage)
            echo "Detecting with 'pytest cov options'"
            PYTEST_COV_OPTIONS="--cov-branch --cov --cov-report="
            BRITNEY=./ci/britney-coverage.sh
            TEST_FLAGS=
            shift
            ;;
        (--branch)
            echo "Requested to try and use branch $2"
            BRANCH="$2"
            shift 2
            ;;
        (*)
            echo "Unknown option"
            exit 255
    esac
done
export TEST_FLAGS

# We're going to *try* to clone the requested branch, but if that fails
# (because it doesn't exist) we're just cloning the default branch.
git clone -b $BRANCH --depth=1 $SUBMODULES https://salsa.debian.org/debian/britney2-tests.git britney2-tests || \
    git clone --depth=1 $SUBMODULES https://salsa.debian.org/debian/britney2-tests.git britney2-tests

if [ -z "$SUBMODULES" ]; then
    py.test-3 -v $PYTEST_COV_OPTIONS || err=$?
fi

echo
britney2-tests/bin/runtests "$BRITNEY" $TEST_SUITE test-out $TEST || err=$?

if [ -n "$TEST" ] && [ $err != 0 ] ; then
    echo
    echo "Content of diff(s)"
    cat test-out/${TEST}*/diff
    echo
    echo "Content of excuses.diff(s)"
    cat test-out/${TEST}*/excuses.diff 2> /dev/null || echo "  none present"
fi

if [ -z "$TEST_FLAGS" ]; then
    python3-coverage report -m
    echo
    python3-coverage html -d coverage
    mkdir codestyle
    pycodestyle --config=setup.cfg britney.py  britney2  --show-source --count > codestyle/codestyle.txt || :
fi

exit $err
